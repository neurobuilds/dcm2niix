ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG DCM2NIIX_VERSION=1.0.20211006
ARG N_BUILD_THREADS=4


FROM ${BASE_DISTRO}:${BASE_VERSION} as apt-base
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
    apt-get install -y -q \
           gcc \
           g++ \
           cmake \
           git \
           pigz


FROM ${BASE_DISTRO}:${BASE_VERSION} as yum-base
ARG N_BUILD_THREADS

RUN yum install -y -q \
           gcc-c++ \
           make \
           git \
           openssl-devel \
           libstdc++-static \
           pigz

RUN cd /opt && \
    curl -L --silent https://cmake.org/files/v3.20/cmake-3.20.4.tar.gz > cmake-3.20.4.tar.gz && \
    tar -xzf cmake-3.20.4.tar.gz && \
    cd cmake-3.20.4 && \
    ./bootstrap --parallel=${N_BUILD_THREADS} && \
    make -j${N_BUILD_THREADS} && \
    make install


FROM ${PACKAGE_MANAGER}-base as install-dcm2niix
ARG DCM2NIIX_VERSION
ARG N_BUILD_THREADS

# Install dcm2niix
RUN mkdir -p /tmp/dcm2niix-src && \
    cd /tmp/dcm2niix-src && \
	git clone https://github.com/rordenlab/dcm2niix.git && \
	cd dcm2niix && \
	git checkout tags/v${DCM2NIIX_VERSION} && \
	mkdir -p build && \
	cd build && \
	cmake \
	    -DBATCH_VERSION=ON \
	    -DUSE_OPENJPEG=ON \
	    -DUSE_GIT_PROTOCOL=OFF \
	    -DCMAKE_INSTALL_PREFIX=/opt/dcm2niix \
	    .. && \
	make -j${N_BUILD_THREADS} && \
	make install
RUN echo -e "{\
    \n  \"build_version\": \"$DCM2NIIX_VERSION\" \
    \n}" > /opt/dcm2niix/manifest.json


FROM ${BASE_DISTRO}:${BASE_VERSION} as final
LABEL maintainer=blake.dewey@jhu.edu
# Copy Build Artifacts
COPY --from=install-dcm2niix /opt/dcm2niix /opt/dcm2niix
COPY --from=install-dcm2niix /usr/bin/pigz /opt/dcm2niix/bin

# Update Environment Variables
ENV PATH /opt/dcm2niix/bin:${PATH}

CMD ["/bin/bash"]